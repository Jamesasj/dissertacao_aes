# Executa modelo da competencia 3 com features do pEssay(celso)
require(corrplot)
require(dplyr)
require(tibble)
require(caret)
require("Metrics")
require(tidyr)
source("lib/Util.R")

features <- read.csv("arquivos/dataset2.csv", header = F, stringsAsFactors = F)

#files.saida <- "resultados1.txt"
#removerZeros = F

temp <- features %>% 
  separate(V1, c("n1","tema","n2", "avaliado")) %>%
  mutate(n1 = NULL, n2 = NULL,V162 = NULL, V163 = NULL, V164= NULL, 
         V165= NULL, tema = as.integer(tema), avaliado = as.integer(avaliado))


dataset <- read.csv("arquivos/dataset.csv", header = F, sep = " ")
f.nomes <- read.csv("arquivos/featuresnames", header = F)
colnames(dataset) <- f.nomes[,1]

temp2 <- dataset %>% left_join(temp, by = c("tema", "avaliado"))
temp3 <- temp2 %>% 
  filter(!is.na(B_LOCAL)) %>%
  mutate(tema = NULL, avaliado=NULL)

if(removerZeros ){
  temp3 <- temp3 %>% filter(!(nota == 0))
}

f = formula(
  paste("nota", paste(colnames(temp3[,-6]), collapse = "+"),
        sep = "~")
)

modelo = lm(f, temp3)
features <- temp3[,-6]
predicao <- data.frame(predicao = predict(modelo, features))
predicao <- predicao %>% mutate(ajuste = if_else(predicao < 0.25, 0, 
                                                 if_else(predicao > 0.25 & predicao < 0.75, .5,
                                                         if_else(predicao > 0.75 & predicao < 1.25, 1,
                                                                 if_else(predicao > 1.25 & predicao < 1.75, 1.5, 2))
                                                 )
))

predicao <- predicao$ajuste
labels <- temp3[,6]
pr.base <- rep(1.0, length(labels))

res1 <- confusionMatrixAtK(predicao, labels,0.0)

res.QWK <- round(ScoreQuadraticWeightedKappaAtK(labels, predicao, 0.0), 3)
res.WK <- round(res1$overall['Kappa'], 3)
res.ACC < round(res1$overall['Accuracy'], 3)


res.PER =  round(cor(predicao, labels, method = 'pearson'), 3)
res.SPE =  round(cor(predicao, labels, method = 'spearman'), 3)

lsaida <- t(c("NER+pEssay", res.ACC , res.QWK, res.WK, res.PER, res.SPE))
write.table(lsaida, append = T, file = files.saida,col.names = F, row.names = F, quote = F,sep = ",")


