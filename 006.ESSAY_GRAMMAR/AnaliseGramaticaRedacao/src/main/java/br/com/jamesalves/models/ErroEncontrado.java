package br.com.jamesalves.models;

public class ErroEncontrado {

	private String sentenca;
	private String regra;
	private String mensagem;
	private String[] sugestao;

	public ErroEncontrado(String sentenca, String regra, String mensagem, String[] sugestao) {
		this.sentenca = sentenca;
		this.regra = regra;
		this.mensagem = mensagem;
		this.sugestao = sugestao;
	}
	
	

	public ErroEncontrado(StringBuilder erros) {
		this.sentenca = null;
		this.regra = null;
		this.mensagem = null;
		this.sugestao = null;
	}



	public String getSentenca() {
		return sentenca;
	}

	public void setSentenca(String sentenca) {
		this.sentenca = sentenca;
	}

	public String getRegra() {
		return regra;
	}

	public void setRegra(String regra) {
		this.regra = regra;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String[] getSugestao() {
		return sugestao;
	}

	public void setSugestao(String[] sugestao) {
		this.sugestao = sugestao;
	}

}
