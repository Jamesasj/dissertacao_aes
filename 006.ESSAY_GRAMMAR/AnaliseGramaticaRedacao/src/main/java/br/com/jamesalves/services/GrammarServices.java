package br.com.jamesalves.services;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.math.NumberUtils;
import org.cogroo.analyzer.Analyzer;
import org.cogroo.analyzer.ComponentFactory;
import org.cogroo.checker.CheckDocument;
import org.cogroo.checker.GrammarChecker;
import org.cogroo.entities.Mistake;
import org.cogroo.text.Sentence;

import br.com.jamesalves.models.ErroEncontrado;

public class GrammarServices {
	private static ComponentFactory fac;
	private static Analyzer cog;
	private CheckDocument document;
	private int[] lErrosGramaticais;
//	private StringBuilder lErrosTexto;
	private List<ErroEncontrado>lErrosTexto;
	
	public GrammarServices() {
		this.fac = ComponentFactory.create(new Locale("pt", "BR"));
		this.cog = fac.createPipe();
		criarNovaLista();
	}

	private void criarNovaLista() {
		this.lErrosGramaticais = new int[129];
	}

	public int[] getErrosGramaticais() {
		return lErrosGramaticais;
	}

	// Extrai as 124 features do gramaticais cogroo
	public void analisarGramatica(String texto) {
		criarNovaLista();
		try {
			GrammarChecker gc = new GrammarChecker(cog);
			document = new CheckDocument(texto);
			gc.analyze(document);
			List<Mistake> lErros = document.getMistakes();
			lErros.stream().filter(erro -> NumberUtils.isNumber(erro.getRuleIdentifier().split(":")[1])).forEach(
					erro -> this.lErrosGramaticais[Integer.parseInt(erro.getRuleIdentifier().split(":")[1]) - 1]++);
		} catch (IllegalArgumentException e) {
			this.lErrosGramaticais[0] = -1;
		} catch (IOException e) {
			this.lErrosGramaticais[0] = -1;
		} catch (StringIndexOutOfBoundsException e) {
			this.lErrosGramaticais[0] = -1;
		}
	}
	
	public void analisarGramaticaWhitErros(String texto) {
		criarNovaLista();
		try {
			GrammarChecker gc = new GrammarChecker(cog);
			document = new CheckDocument(texto);
			gc.analyze(document);
//			StringBuilder erros = new StringBuilder();
			List<ErroEncontrado> erros = new ArrayList<>();
			List<Sentence> lSentenca = document.getSentences();
			
			for (Sentence sentence : lSentenca) {
				GrammarChecker gcs = new GrammarChecker(cog);
				CheckDocument stdoc = new CheckDocument(sentence.getText());
				gcs.analyze(stdoc);
				
				List<Mistake> lStErros = stdoc.getMistakes();
				
				lStErros.stream().filter(erro -> NumberUtils.isNumber(erro.getRuleIdentifier().split(":")[1])).forEach(
						erro -> this.lErrosGramaticais[Integer.parseInt(erro.getRuleIdentifier().split(":")[1]) - 1]++);
				
				for (Mistake mistake : lStErros) {
					String[] sugestao = mistake.getSuggestions();
					String mensagem = mistake.getShortMessage();
					String regra = mistake.getRuleIdentifier();
					String sentenca = mistake.getContext();
					ErroEncontrado tempErro = new ErroEncontrado(sentenca, regra, mensagem, sugestao);
					erros.add(tempErro);
/*					erros.append("sentenca: " +mistake.getContext()).append("\n")
					.append("regra: "+mistake.getRuleIdentifier()).append("\n")
					.append("mensagem:"+mistake.getShortMessage()).append("\n")
					.append("sugestoes:"+Arrays.toString(mistake.getSuggestions())).append("\n")
					.append("-").append("\n");*/
				}
				
			}
			this.setlErrosTexto(erros);	
		} catch (IllegalArgumentException e) {
			this.lErrosGramaticais[0] = -1;
		} catch (IOException e) {
			this.lErrosGramaticais[0] = -1;
		} catch (StringIndexOutOfBoundsException e) {
			this.lErrosGramaticais[0] = -1;
		} catch(NullPointerException e){
			this.lErrosGramaticais[0] = -1;
		}
	}

	public List<ErroEncontrado> getlErrosTexto() {
		return lErrosTexto;
	}

	public void setlErrosTexto(List<ErroEncontrado> lErrosTexto) {
		this.lErrosTexto = lErrosTexto;
	}

}
