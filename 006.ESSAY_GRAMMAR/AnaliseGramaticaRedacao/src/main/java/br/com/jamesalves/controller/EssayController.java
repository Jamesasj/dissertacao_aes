package br.com.jamesalves.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.jamesalves.domains.Redacao;
import br.com.jamesalves.domains.RedacaoWrapper;
import br.com.jamesalves.models.Documento;
import br.com.jamesalves.models.ErroEncontrado;
import br.com.jamesalves.services.GeneralFeaturesServices;
import br.com.jamesalves.services.GrammarServices;
import br.com.jamesalves.services.TaggerServices;

@RestController
@RequestMapping("/essay")
public class EssayController {

	@GetMapping("/testes")
	public ResponseEntity<String> testeResposta() {
		return new ResponseEntity<String>("Funcionamento OK!", null, HttpStatus.OK);
	}

	/*
	 * @Deprecated
	 * 
	 * @PostMapping("/geral") public ResponseEntity<Map<String, Double>>
	 * analisarGeneral(@RequestBody Redacao essay) { GeneralFeaturesServices
	 * featuresServices = new GeneralFeaturesServices();
	 * featuresServices.analisarFeatures(essay.getTexto()); ///Map<String, Double> f
	 * = featuresServices.getLfeatures(); return new ResponseEntity<Map<String,
	 * Double>>(f, null, HttpStatus.OK); }
	 */
	@PostMapping("/gramatica")
	public ResponseEntity<int[]> analisarGrammar(@RequestBody Redacao essay) {
		GrammarServices services = new GrammarServices();
		services.analisarGramatica(essay.getTexto());
		int[] f = services.getErrosGramaticais();
		return new ResponseEntity<>(f, null, HttpStatus.OK);
	}

	@PostMapping("/postag")
	public ResponseEntity analisarPostag(@RequestBody Redacao essay) {
		TaggerServices services = new TaggerServices();
		try {
			services.extrairTagger(essay.getTexto());
			Map<String, Integer> f = services.getlFeatures();
			return new ResponseEntity(f, null, HttpStatus.OK);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ResponseEntity("Falhar ao extrair features", null, HttpStatus.OK);
	}
	/*
	 * @Deprecated
	 * 
	 * @PostMapping(value = "listaGeral", consumes = "application/json", produces =
	 * "application/json") public ResponseEntity analisarListaGeral(@RequestBody
	 * RedacaoWrapper redacaoWrapper) { GeneralFeaturesServices featuresServices =
	 * new GeneralFeaturesServices(); Map<String, Map> lfeatures = new
	 * HashMap<String, Map>(); for (Redacao essay : redacaoWrapper.getRedacoes()) {
	 * featuresServices.analisarFeatures(essay.getTexto()); //Map<String, Double> f
	 * = featuresServices.getLfeatures(); ////lfeatures.put(essay.getNomeArquivo(),
	 * f); } return new ResponseEntity<Map<String, Map>>(lfeatures, null,
	 * HttpStatus.OK); }
	 */
	
	@PostMapping(value = "listaGramatica", consumes = "application/json", produces = "application/json")
	public ResponseEntity analisarListaGrammar(@RequestBody RedacaoWrapper redacaoWrapper) {
		GrammarServices services = new GrammarServices();
		Map<String, int[]> lfeatures = new HashMap<String, int[]>();
		for (Redacao essay : redacaoWrapper.getRedacoes()) {
			services.analisarGramatica(essay.getTexto());
			int[] f = services.getErrosGramaticais();
			lfeatures.put(essay.getNomeArquivo(), f);
		}
		return new ResponseEntity<>(lfeatures, null, HttpStatus.OK);
	}

	@PostMapping(value = "listaGramatica_v2", consumes = "application/json", produces = "application/json")
	public ResponseEntity analisarListaGrammarV2(@RequestBody RedacaoWrapper redacaoWrapper) {
		GrammarServices services = new GrammarServices();
		Map<String, Documento> lfeatures = new HashMap<String, Documento>();
		for (Redacao essay : redacaoWrapper.getRedacoes()) {
			services.analisarGramaticaWhitErros(essay.getTexto());
			int[] f = services.getErrosGramaticais();
			List<ErroEncontrado> e = services.getlErrosTexto();
			Documento doc = new Documento(f,e,essay.getNomeArquivo());
			lfeatures.put(essay.getNomeArquivo(), doc);
		}
		return new ResponseEntity<>(lfeatures, null, HttpStatus.OK);
	}
	
	@PostMapping(value = "listaGeral_v2", consumes = "application/json", produces = "application/json")
	public ResponseEntity analisarListaGeralV2(@RequestBody RedacaoWrapper redacaoWrapper) {
		GeneralFeaturesServices featuresServices = new GeneralFeaturesServices();
		Map<String, Documento> lfeatures = new HashMap<String, Documento>();
		
		for (Redacao essay : redacaoWrapper.getRedacoes()) {
			featuresServices.analisarFeatures(essay.getTexto());
			Double[] f = featuresServices.getLfeatures();
			StringBuilder e = featuresServices.getlErrosTexto();
			Documento doc = new Documento(f, e, essay.getNomeArquivo());
			lfeatures.put(essay.getNomeArquivo(), doc);
		}
		return new ResponseEntity<Map<String, Documento>>(lfeatures, null, HttpStatus.OK);
	}



	@PostMapping(value = "listaPostag", consumes = "application/json", produces = "application/json")
	public ResponseEntity analisarListaPostag(@RequestBody RedacaoWrapper redacaoWrapper) {
		try {
			TaggerServices services = new TaggerServices();
			Map<String, int[]> lfeatures = new HashMap<String, int[]>();
			for (Redacao essay : redacaoWrapper.getRedacoes()) {
				try {
					services.extrairTagger(essay.getTexto());
					int[] f = services.getlFeaturesValues();
					lfeatures.put(essay.getNomeArquivo(), f);
				} catch (NullPointerException e) {
					lfeatures.put(essay.getNomeArquivo(), new int[services.getLengthFeaturesVet()] );
				}

			}
			return new ResponseEntity(lfeatures, null, HttpStatus.OK);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ResponseEntity("Falhar ao extrair features", null, HttpStatus.OK);
	}

	
	@PostMapping(value = "listaPostag_v2", consumes = "application/json", produces = "application/json")
	public ResponseEntity analisarListaPostagV2(@RequestBody RedacaoWrapper redacaoWrapper) {
		try {
			TaggerServices services = new TaggerServices();
			Map<String, Documento> lfeatures = new HashMap<String, Documento>();
			for (Redacao essay : redacaoWrapper.getRedacoes()) {
				try {
					services.extrairTagger(essay.getTexto());
					int[] f = services.getlFeaturesValues();
					Documento doc = new Documento(f, new StringBuilder(""), essay.getNomeArquivo());
					lfeatures.put(essay.getNomeArquivo(), doc);
				} catch (NullPointerException e) {
					Documento doc = new Documento(new int[services.getLengthFeaturesVet()], new StringBuilder("Erro de extracao"), essay.getNomeArquivo());
					lfeatures.put(essay.getNomeArquivo(),doc  );
				}
			}
			return new ResponseEntity(lfeatures, null, HttpStatus.OK);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ResponseEntity("Falhar ao extrair features", null, HttpStatus.OK);
	}

}
