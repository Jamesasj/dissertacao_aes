package br.com.jamesalves.services;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;

public class TaggerServices {

	private Map<String, Integer> lFeatures;
	private String[] vetFeaturesNames;
	public int getLengthFeaturesVet() {
		return this.vetFeaturesNames.length;
	}
	public TaggerServices() {
		criarNovaLista();
	}

	private void criarNovaLista() {
		vetFeaturesNames = new String[] { "adj", "adv", "art", "conj-c", "conj-s", "ec", "n", "num", "pp", "pron-det", "pron-indp",
				"pron-pers", "prop", "prp", "punc", "v-fin", "v-ger", "v-inf", "vp", "v-pcp" };
		this.lFeatures = new HashMap<>();

		for (int i = 0; i < vetFeaturesNames.length; i++) {
			this.lFeatures.put(vetFeaturesNames[i], 0);
		}
	}

	public Map<String, Integer> getlFeatures() {
		return lFeatures;
	}
	
	public int [] getlFeaturesValues() {
		int[] aux = new int[this.vetFeaturesNames.length];
		for (int i = 0; i < aux.length; i++) {
			aux[i] = lFeatures.get(this.vetFeaturesNames[i]);
		}
		return aux;
	}

	private static void extrairSentenca() throws FileNotFoundException, IOException {
		try (FileInputStream modeloEntrada = new FileInputStream("pt-sent.bin")) {
			SentenceModel modelo = new SentenceModel(modeloEntrada);
			SentenceDetectorME detectorSenten = new SentenceDetectorME(modelo);
			String[] x = detectorSenten
					.sentDetect("Testes de doken do dr. James. Isso pode funcionar, porem pode falhar miseravelmente.");
			System.out.println(Arrays.toString(x));
		}

	}

	private String[] extrairToken(String texto) throws IOException, FileNotFoundException {
		this.criarNovaLista();
		try (FileInputStream modeloEntrada = new FileInputStream("pt-token.bin")) {
			TokenizerModel modelo = new TokenizerModel(modeloEntrada);
			TokenizerME tokenizador = new TokenizerME(modelo);
			String tokens[] = tokenizador.tokenize(texto);
			return tokens;
		}
	}

	public String[] extrairTagger(String texto) throws FileNotFoundException, IOException {
		try (InputStream modelIn = new FileInputStream("pt-pos-perceptron.bin")) {
			POSModel model = new POSModel(modelIn);
			POSTaggerME tagger = new POSTaggerME(model);
			String tags[] = tagger.tag(extrairToken(texto));
			for (int i = 0; i < tags.length; i++) {
				this.lFeatures.put(tags[i], this.lFeatures.get(tags[i]) + 1);
			}
			return (tags);
		}
	}

	public static void main(String[] args) throws IOException {
		String texto = "O legado da Ciência\n"
				+ "O mundo europeu do século XVI e XVII passava por grandes transformações."
				+ " A estrutura do pensamento vigente, as concepsões defendidas pela Igreja "
				+ "e pela filosofia, estavam sendo transformadas por movimentos como o "
				+ "Antropocentrismo, o Renascentismo dentre outros. A ciencia estava nascendo, "
				+ "divorciada da filosofia, tendo seu próprio método para produzir o conhecimento. "
				+ "A micro visão de que o mundo era imóvel e hierarquizado, ampliava-se agora para "
				+ "uma visão de um universo infinito.\n"
				+ "O conhecimento científico foi se transformando numa prática constante, "
				+ "andando lado a lado com a tecnologia para produzir um conhecimento objetivo, "
				+ "preciso e sistemático. Sendo assim seria o legado da ciencia garantir nossa "
				+ "sobrevivência no mundo atual?\n"
				+ "Desde a Revolução Industrial, período em que começou a globalizassão, "
				+ "sendo intensificado após a Segunda Guerra Mundial, a ciencia mostra "
				+ "resultados significativos de sua aplicação em áreas como: "
				+ "informática, medicina, tecnologias de comunicação e de transportes. "
				+ "Através de invenções como a eletricidade, o telefone, a vacina, "
				+ "os antibioticos, o ar-condicionado, o carro, a geladeira e muitos outros, "
				+ "podemos constatar que todos essas descobertas e invenções, geraram um conjunto "
				+ "de beneficios e avanços que resultaram no aumento da população humana e no aumento "
				+ "da expectativa de vida. 2019\n"
				+ "Não podemos negar a ciencia, não podemos negar os beneficios que ela nos trouxe. "
				+ "Como imaginar nossa sociedade sem todos os aparatos, confortos, facilidades e "
				+ "invenções que a ciencia nos legou? Voltaríamos as crenças, a ignorância, a "
				+ "barbárie, aos mitos, a falta de esperança diante das doenças e limitações "
				+ "humanas? Tanto a ciencia quanto a tecnologia seguem sendo instrumentos "
				+ "revolucionários para garantir a qualidade de vida e a mais que " + "sobrevivência de nossa espécie.";

		// extracaoDeToken();
		// extrairSentenca();
		TaggerServices srv = new TaggerServices();
		srv.extrairTagger(texto);
		System.out.println(srv.getlFeatures());

	}

}
