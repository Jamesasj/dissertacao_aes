package br.com.jamesalves.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Documento {

	private List<ErroEncontrado> erros;
	private String features;

	public List<ErroEncontrado> getErros() {
		return erros;
	}

	public String getFeatures() {
		return features;
	}

/*	public Documento(int[] features, StringBuilder erros, String nomeArquivo) {
		this.erros = erros.toString();
		this.features = nomeArquivo + "," + Arrays.toString(features).replace("[", "").replace("]", "");
	}

	public Documento(Double[] features, StringBuilder erros, String nomeArquivo) {
		this.erros = erros.toString();
		this.features = nomeArquivo + "," + Arrays.toString(features).replace("[", "").replace("]", "");
	}*/
	
	public Documento(int[] features, List<ErroEncontrado> erros, String nomeArquivo) {
		this.erros = erros;
		this.features = nomeArquivo + "," + Arrays.toString(features).replace("[", "").replace("]", "");
	}
	
	public Documento(Double[] features, StringBuilder erros, String nomeArquivo) {
		this.erros = new ArrayList<>();
		this.features = nomeArquivo + "," + Arrays.toString(features).replace("[", "").replace("]", "");
	}


	public Documento(int[] features, StringBuilder erros, String nomeArquivo) {
		this.erros = new ArrayList<>();
		this.features = nomeArquivo + "," + Arrays.toString(features).replace("[", "").replace("]", "");
	}
	
	@Override
	public String toString() {
		return "Documento [erros=" + erros + ", features=" + features + "]";
	}

}
