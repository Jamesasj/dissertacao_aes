package br.com.jamesalves.services;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import org.cogroo.analyzer.Analyzer;
import org.cogroo.analyzer.ComponentFactory;
import org.cogroo.checker.CheckDocument;
import org.cogroo.checker.GrammarChecker;

public class GeneralFeaturesServices {
	private static final ComponentFactory fac = ComponentFactory.create(new Locale("pt", "BR"));
	private static final Analyzer cog = fac.createPipe();
	private GrammarChecker CorGrammar;
	private CheckDocument documento;
	private Map<String, Double> lfeatures;
	private static final String TOTAL_TOKENS = "total_tokens";
	private static final String TOTAL_SENTENCAS = "total_sentencas";
	private static final String TOTAL_ERROS = "total_erros";
	private static final String TOTAL_SENTENCAS_GRANDES = "total_sentencas_grandes";
	private static final String MEDIA_TOKENS = "media_tokens";
	private static final String MEDIA_ERROS = "media_erros_gramaticais_cogroo";
	private static final String TOTAL_PARAGRAFOS = "total_paragrafos";
	private static final String MEDIA_SENTENCAS = "media_sentencas";

	public Double[] getLfeatures() {
		Double[] ret = new Double[8];
		ret[0] = lfeatures.get(TOTAL_TOKENS);
		ret[1] = lfeatures.get(TOTAL_SENTENCAS);
		ret[2] = lfeatures.get(TOTAL_ERROS);
		ret[3] = lfeatures.get(TOTAL_SENTENCAS_GRANDES);
		ret[4] = lfeatures.get(MEDIA_TOKENS);
		ret[5] = lfeatures.get(MEDIA_ERROS);
		ret[6] = lfeatures.get(TOTAL_PARAGRAFOS);
		ret[7] = lfeatures.get(MEDIA_SENTENCAS);
		return ret ;
	}

	public GeneralFeaturesServices() {
		try {
			this.CorGrammar = new GrammarChecker(cog);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void criarNovaLista() {
		this.lfeatures = new HashMap<String, Double>();
		this.lfeatures.put(GeneralFeaturesServices.TOTAL_TOKENS, 0.0);
		this.lfeatures.put(GeneralFeaturesServices.TOTAL_SENTENCAS, 0.0);
		this.lfeatures.put(GeneralFeaturesServices.TOTAL_ERROS, 0.0);
		this.lfeatures.put(GeneralFeaturesServices.TOTAL_SENTENCAS_GRANDES, 0.0);
		this.lfeatures.put(GeneralFeaturesServices.MEDIA_TOKENS, 0.0);
		this.lfeatures.put(GeneralFeaturesServices.MEDIA_ERROS, 0.0);
	}

	public void analisarFeatures(String texto) {
		this.criarNovaLista();

		this.documento = new CheckDocument(texto);
		try {
				this.CorGrammar.analyze(this.documento);
		
				this.lfeatures.put(GeneralFeaturesServices.TOTAL_SENTENCAS,
						this.documento.getSentences().size() + this.lfeatures.get(GeneralFeaturesServices.TOTAL_SENTENCAS));
		
				this.lfeatures.put(GeneralFeaturesServices.TOTAL_ERROS,
						this.documento.getMistakes().size() + this.lfeatures.get(GeneralFeaturesServices.TOTAL_ERROS));
		
				this.documento.getSentences().stream().forEach(sentenca -> {
					this.lfeatures.put(GeneralFeaturesServices.TOTAL_TOKENS,
							this.lfeatures.get(GeneralFeaturesServices.TOTAL_TOKENS) + sentenca.getTokens().size());
					this.lfeatures.put(GeneralFeaturesServices.TOTAL_SENTENCAS_GRANDES,
							this.lfeatures.get(GeneralFeaturesServices.TOTAL_SENTENCAS_GRANDES)
									+ sentenca.getTokens().size() >= 70 ? 1.0 : 0.0);
				});
		
				this.lfeatures.put(GeneralFeaturesServices.MEDIA_TOKENS,
						this.lfeatures.get(TOTAL_TOKENS) / this.lfeatures.get(TOTAL_SENTENCAS));
		
				this.lfeatures.put(GeneralFeaturesServices.MEDIA_ERROS,
						this.lfeatures.get(TOTAL_ERROS) / this.lfeatures.get(TOTAL_TOKENS));
		
				this.lfeatures.put(TOTAL_PARAGRAFOS, Double.parseDouble(texto.split("\n").length + ""));
		
				this.lfeatures.put(MEDIA_SENTENCAS, this.lfeatures.get(TOTAL_SENTENCAS) / this.lfeatures.get(TOTAL_PARAGRAFOS));
		} catch (StringIndexOutOfBoundsException e) {
			this.lfeatures.put(GeneralFeaturesServices.TOTAL_TOKENS,-1.0);
		} catch(NullPointerException e) {
			this.lfeatures.put(GeneralFeaturesServices.TOTAL_TOKENS,-2.0);
		} catch(IndexOutOfBoundsException e) {
			this.lfeatures.put(GeneralFeaturesServices.TOTAL_TOKENS,-3.0);	
		}
	}

	public static void main(String[] args) {
		GeneralFeaturesServices x = new GeneralFeaturesServices();
		x.analisarFeatures(
				"A fazem-se persistência da violência contra a mulher na sociedade brasileira é um problema muito presente. "
						+ "Isso deve ser enfrentado, uma vez que, diariamente, mulheres são vítimas dessa questão. Nesse sentido, "
						+ "dois aspectos fazem-se relevantes: o legado histórico cultural e o desrespeito às leis.\r\n"
						+ "Segundo a História, a mulher sempre foi vista como inferior e submissa ao homem. "
						+ "Comprova-se isso pelo fato de elas poderem exercer direitos políticos, ingressarem no mercado de "
						+ "trabalho e escolherem suas próprias roupas muito tempo depois do gênero oposto. Esse cenário, "
						+ "juntamente aos inúmeros casos de violência contra as mulheres, corroboram a ideia de que elas são "
						+ "vítimas de um legado histórico-cultural. Nesse ínterim, a cultura machista prevaleceu ao longo dos "
						+ "anos a ponto de enraizar-se na sociedade contemporânea, mesmo que de forma implícita, à primeira vista.\r\n"
						+ "Conforme previsto pela Constituição Brasileira, todos são iguais perante à lei, "
						+ "independente de cor, raça ou gênero, sendo a isonomia salarial, aquela que prevê mesmo "
						+ "salário para os que desempenham mesma função, também garantida por lei. No entanto, o que se "
						+ "observa em diversas partes do país, é a gritante diferença entre os salários de homens e mulheres, "
						+ "principalmente se estas foram negras. Esse fato causa extrema decepção e constrangimento a elas, as "
						+ "quais sentem-se inseguras e sem ter a quem recorrer. Desse modo, medidas fazem-se necessárias para solucionar a problemática.\r\n"
						+ "Diante dos argumentos supracitados, é dever do Estado proteger as mulheres da violência, "
						+ "tanto física quanto moral, criando campanhas de combate à violência, além de impor leis mais "
						+ "rígidas e punições mais severas para aqueles que não as cumprem. Some-se a isso "
						+ "investimentos em educação, valorizando e capacitando os professores, no intuito de formar "
						+ "cidadãos mais comprometidos em garantir o bem-estar da sociedade como um todo.");
		System.out.println(x.getLfeatures());
		/*
		 * GrammarServices y = new GrammarServices();
		 * y.analisarGramatica("Ontem nós fui as praia.");
		 * System.out.println(Arrays.toString(y.getErrosGramaticais()));
		 */
	}

	public StringBuilder getlErrosTexto() {
		return new StringBuilder("");
	}

}
