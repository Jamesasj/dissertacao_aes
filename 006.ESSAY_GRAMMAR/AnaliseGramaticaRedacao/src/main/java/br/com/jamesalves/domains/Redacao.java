package br.com.jamesalves.domains;

import java.io.Serializable;

import org.springframework.stereotype.Component;

@Component
public class Redacao implements Serializable {
	private String texto;
	private String nomeArquivo;

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public String getNomeArquivo() {
		return nomeArquivo;
	}

	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}

}
