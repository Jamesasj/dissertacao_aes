package br.com.jamesalves;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AnaliseGramaticaRedacaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnaliseGramaticaRedacaoApplication.class, args);
	}

}
