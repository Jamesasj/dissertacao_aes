package br.com.jamesalves.services;

import static org.junit.jupiter.api.Assertions.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;

import org.junit.jupiter.api.Test;

class TaggerServicesTest {
	String TEXTO = "O legado da Ciência\r\n"
			+ "O mundo europeu do século XVI e XVII passava por grandes transformações."
			+ " A estrutura do pensamento vigente, as concepsões defendidas pela Igreja "
			+ "e pela filosofia, estavam sendo transformadas por movimentos como o "
			+ "Antropocentrismo, o Renascentismo dentre outros. A ciencia estava nascendo, "
			+ "divorciada da filosofia, tendo seu próprio método para produzir o conhecimento. "
			+ "A micro visão de que o mundo era imóvel e hierarquizado, ampliava-se agora para "
			+ "uma visão de um universo infinito.\n"
			+ "O conhecimento científico foi se transformando numa prática constante, "
			+ "andando lado a lado com a tecnologia para produzir um conhecimento objetivo, "
			+ "preciso e sistemático. Sendo assim seria o legado da ciencia garantir nossa "
			+ "sobrevivência no mundo atual?\n"
			+ "Desde a Revolução Industrial, período em que começou a globalizassão, "
			+ "sendo intensificado após a Segunda Guerra Mundial, a ciencia mostra "
			+ "resultados significativos de sua aplicação em áreas como: "
			+ "informática, medicina, tecnologias de comunicação e de transportes. "
			+ "Através de invenções como a eletricidade, o telefone, a vacina, "
			+ "os antibioticos, o ar-condicionado, o carro, a geladeira e muitos outros, "
			+ "podemos constatar que todos essas descobertas e invenções, geraram um conjunto "
			+ "de beneficios e avanços que resultaram no aumento da população humana e no aumento "
			+ "da expectativa de vida.\n"
			+ "Não podemos negar a ciencia, não podemos negar os beneficios que ela nos trouxe. "
			+ "Como imaginar nossa sociedade sem todos os aparatos, confortos, facilidades e "
			+ "invenções que a ciencia nos legou? Voltaríamos as crenças, a ignorância, a "
			+ "barbárie, aos mitos, a falta de esperança diante das doenças e limitações "
			+ "humanas? Tanto a ciencia quanto a tecnologia seguem sendo instrumentos "
			+ "revolucionários para garantir a qualidade de vida e a mais que " + "sobrevivência de nossa espécie.";

	@Test
	void testExtrairTagger() throws FileNotFoundException, IOException {
		TaggerServices services = new TaggerServices();
		services.extrairTagger(TEXTO);
		Map<String, Integer> f = services.getlFeatures();
		System.out.println(f);
	}

}
