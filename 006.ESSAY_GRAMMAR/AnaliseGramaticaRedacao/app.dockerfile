FROM openjdk:8-jdk-alpine
WORKDIR /App
VOLUME ["/tmp", "/App"]
ARG JAR_FILE
COPY target/App-0.0.1.jar App.jar
COPY dependencias/ .
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","App.jar"]
#docker run -p 80:8080 --rm -d essaynode:latest
#docker build --rm -f "app.dockerfile" -t essaynode:latest "."