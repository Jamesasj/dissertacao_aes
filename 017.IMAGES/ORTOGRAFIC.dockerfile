FROM python:3
WORKDIR /usr/src/app
COPY ortografic/ .
RUN pip install --no-cache-dir -r requirements.txt
RUN python requirements.py
EXPOSE 8081
CMD [ "python", "./app.py" ]