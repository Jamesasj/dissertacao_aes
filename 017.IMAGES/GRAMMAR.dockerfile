FROM openjdk:8-jdk-alpine
WORKDIR /App
VOLUME ["/App"]
ARG JAR_FILE
COPY grammar/ .
EXPOSE 8080
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","App.jar"]
#docker run -p 80:8080 --rm -d essaynode:latest
#docker build --rm -f "app.dockerfile" -t essaynode:latest "."