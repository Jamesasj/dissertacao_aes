FROM ubuntu:latest
RUN apt-get update && \
    apt-get install -y software-properties-common && \
    rm -rf /var/lib/apt/lists/*

ENV JAVA_VER 8
ENV JAVA_HOME /usr/lib/jvm/java-8-oracle