from hunspell import Hunspell
"""
https://pypi.org/project/CyHunspell/
Baixar pra usar o dicionário pb-BR https://github.com/wesleyz/hunspell.git
"""
h = Hunspell('pt_BR', hunspell_data_dir='dict/')
print(h.spell('Jurisprudência'))
print(h.suggest('Jurisprudência'))
