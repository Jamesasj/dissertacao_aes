 # -*- coding: utf-8 -*
from flask import Flask, escape, request, jsonify,json
from nltk.corpus import stopwords
from string import punctuation
from hunspell import Hunspell
from nltk.tokenize import word_tokenize
stopwords = set(stopwords.words('portuguese') + list(punctuation))
h = Hunspell('pt_BR', hunspell_data_dir='dict/')

app = Flask(__name__)

@app.route('/avaliarErros', methods=['POST'])
def avaliarErros():
    erros = 0
    redacao = request.form['redacao']
    for palavra in redacao.replace('\\n\\n', ' ').replace('\u0300','').split(' '):
        if len(palavra)>0:
            if h.spell(palavra) == False:
                erros = erros + 1
    return  jsonify({'erros' : erros })

@app.route('/listarErros', methods=['POST'])
def listarErros():
    erros = 0
    lista = []
    redacao = request.form['redacao']
    for palavra in redacao.replace('\\n\\n', ' ').replace('\u0300','').replace('\u2030','').split(' '):
        if len(palavra)>0:
            print(palavra)
            if h.spell(palavra) == False:
                erros = erros + 1
                lista.append((palavra, h.suggest(palavra)))
    return  jsonify({'lista': lista})

def analisarErro(nmArquivo, redacao):
    erros = 0
    for palavra in redacao.replace('\\n\\n', ' ').replace('\u0300','').replace('\u2014','').replace('\u2013','').replace('\u2019','').replace('\u201c','').replace('\u2018','').replace('\u2212','').replace('\u201d','').split(' '):
        if len(palavra)>0:
            if h.spell(palavra) == False:
                erros = erros + 1
    return  [erros] 

@app.route('/essay/listaOrtografia', methods=['POST'])
def analisarListaErros():
    lRedacaoes =  request.get_json()['redacoes']
    lres = {}
    for redacao in lRedacaoes:
        lres[redacao['nomeArquivo']] = analisarErro(redacao['nomeArquivo'], redacao['texto']) 
    return jsonify(lres)


def analisarErroV2(nmArquivo, redacao):
    feature = 0
    # erros = ""
    erros = list()
    redacao=redacao.replace('"','').replace('"','').replace('\u0300','').replace('\u2014','').replace('\u2013','').replace('\u2019','').replace('\u201c','').replace('\u2018','').replace('\u2212','').replace('\u201d','')
    aux_redacao = word_tokenize(redacao.lower())
    lpalavras = [palavra for palavra in aux_redacao if palavra not in stopwords]
    for palavra in lpalavras:
        if len(palavra)>0:
            if h.spell(palavra) == False:
                feature = feature + 1
                #erros = erros + 'palavra:' + palavra + '\nsugestoes: ' + ','.join(h.suggest(palavra)) + '\n-\n'
                erros.append({'palavra' : palavra , 'sugestoes': ','.join(h.suggest(palavra)) })
    return  {'features':nmArquivo+','+str(feature),  'erros': erros}  

@app.route('/essay/listaOrtografia_v2', methods=['POST'])
def analisarListaErrosv2():
    lRedacaoes =  request.get_json()['redacoes']
    lres = {}
    for redacao in lRedacaoes:
        lres[redacao['nomeArquivo']] = analisarErroV2(redacao['nomeArquivo'], redacao['texto']) 
    return jsonify(lres)

if __name__=='__main__':
    app.run('0.0.0.0',debug=True, port=8081)
