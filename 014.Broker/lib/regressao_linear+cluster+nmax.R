library(tibble)
library(dplyr)
library(caret)
library(genalg)
library("Metrics")
source("param.R")
source("lib/Funcoes_Avaliacao.R")
source("lib/util.R")
source("lib/grafico.R")

param.freedom = .0
param.removerZeros = T
output.folder = ifelse(param.removerZeros, yes =  'output/rl_cluster_nmax/', 
                       no = 'output/rl_cluster_nmax_0/')

if(dir.exists(output.folder)){unlink(output.folder, recursive = T)}
dir.create(output.folder)
f.cluster = t(read.csv(clusterFileOutput, sep = ' ', header = F))
dataset = read.csv(datasetFileOutput, sep = ',', header = F, stringsAsFactors = F)
dataset$cluster = f.cluster
rownames(dataset) = dataset$V1
notas = read.csv(scoreSamples, sep = ',', header = F, stringsAsFactors = F, col.names = c('V1','notas'))
dataset = dataset %>% left_join(notas, by = c('V1'))

lista.cluster = order(unique(f.cluster))
resultados = data.frame(predito=c(),labels = c())

for (item.cluste in lista.cluster) {
  cluster.dataset = dataset %>% filter(cluster == item.cluste)
  rownames(cluster.dataset) = cluster.dataset$V1
  
  if(nrow(cluster.dataset) > 2 ){
    #remover redações zeradas
    if (param.removerZeros) {
      cluster.dataset = cluster.dataset %>% filter(notas > 0)
    }
    x = subset(cluster.dataset, select = -c(V1,notas))
    rownames(x) = cluster.dataset$V1
    d = dist(x)
    d = as.data.frame(as.matrix(d))
    dispares = nmax(d, 20)
    xdata = data.frame(V1 = c(), notas = c())
    for (idx in 1:nrow(dispares)) {
      xdata = rbind( xdata, cluster.dataset %>% filter(V1 %in% dispares[idx,]) %>% select(V1, notas))
    }
    
    treino.dataset = cluster.dataset %>% filter(V1 %in% xdata$V1)
    cluster.dataset = cluster.dataset %>% filter(!(V1 %in% xdata$V1))
    amostra = sample(1:length(cluster.dataset$notas), length(cluster.dataset$notas)*0.75)  
    teste.dataset  = cluster.dataset[-amostra,]
    
    funcao = util.formula(subset(treino.dataset,select = -c(V1,notas)))
    modelo = train(funcao,treino.dataset, method = "lm" )
    
    
    write.csv(modelo$resample, file = paste0(output.folder,"/resultados_cv_cluster",item.cluste,".csv"),
              append = T, row.names = F, col.names = F )
    
    predito = test_model(modelo, teste.dataset)
    resultados = rbind(resultados, data.frame(predito= predito ,labels = teste.dataset$notas))
    save(modelo, file = paste0(output.folder,"/modelo_clus_",item.cluste,".bin"))
  }
}

result.QWK = ScoreQuadraticWeightedKappaAtK(resultados$labels, resultados$predito, param.freedom)
result.rmse = rmse(resultados$labels, resultados$predito)


saida = paste(ifelse(param.removerZeros, yes =  "regressao linear+cluster+nmax", no = "regressao linear+cluster+nmax 0"),
              format(result.QWK , digits = 4) , 
              format(result.rmse, digits = 4) , 
              sep = ',')

write.csv(saida, 
          file = paste0(output.folder,"/resultados.csv"), 
          append = F, row.names = F, 
          quote = F)

ExecutarGrafico()