 # -*- coding: utf-8 -*
from flask import Flask, escape, request, jsonify,json,render_template
import requests, json, os, param

app = Flask(__name__)

def executarAnaliseOrtografica(texto):
    list_dataessay = list()
    list_dataessay.append({'texto' :texto, 'nomeArquivo':'requisicao'})
    payload = json.dumps({'redacoes' : list_dataessay})
    response = requests.request("POST",url= param.urlsOrtografic[0], headers=param.headers, data = payload)
    responseJson = response.json()
    erros = responseJson['requisicao']['erros']
    return erros

def executarAnaliseGrammar(texto):
    list_dataessay = list()
    list_dataessay.append({'texto' :texto, 'nomeArquivo':'requisicao'})
    payload = json.dumps({'redacoes' : list_dataessay})
    response = requests.request("POST",url= param.urlsGrammar[0], headers=param.headers, data = payload)
    responseJson = response.json()
    erros = responseJson['requisicao']['erros']
    return erros

@app.route('/')
def hello_world():
    return render_template('index.html')

@app.route('/enviarRedacao', methods=['POST'])
def send_essay():
    redacao = request.form['redacao']
    errosOrtograficos = executarAnaliseOrtografica(redacao)
    errosGramaticais = executarAnaliseGrammar(redacao)
    novo = redacao
    count = 1
    lErros = list()

    for erro in errosGramaticais:
        if('space:EXTRA_BETWEEN_WORDS' != erro['regra']):
            novo = novo.replace(erro['sentenca'],'<span title="Erro {} - Gramatical" class = "grammar" tipo="2" idErro={}>{}</span>'.format(count,count,erro['sentenca']) )
            lErros.append({'idErro':count,'sugestao':erro['sugestao'],'regra':erro['mensagem'],'codRegra':erro['regra'], 'tpErro':'Gramatical', 'classe':'grammar'})
            count = count+1

    for erro in errosOrtograficos:
        novo = novo.replace(erro['palavra'],'<span title="Erro {} - Ortográfico" tipo="1" class = "ortografic" idErro={}>{}</span>'.format(count,count,erro['palavra']))
        lErros.append({'idErro':count,'sugestao':erro['sugestoes'],'regra':'','codRegra':'', 'tpErro':'Ortográfico', 'classe':'ortografic'})
        count = count+1
    return render_template('index.html', texto=novo.split('\n'), lErros=lErros)

if __name__=='__main__':
    app.run('0.0.0.0',debug=True, port=8085)


