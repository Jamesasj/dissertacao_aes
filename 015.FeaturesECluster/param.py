# Nodes de extracao de features
urlsGrammar = ["http://localhost:8080/essay/listaGramatica_v2"]
urlsNLP = ["http://localhost:8080/essay/listaPostag_v2"]
urlsOrtografic = ["http://localhost:8081/essay/listaOrtografia_v2"]
urlsGeneral = ["http://localhost:8080/essay/listaGeral_v2"]
# ENTRADAS
dataset = "redacoes100/" # local do dataset de redacoes
scoreSamples = "essayscoresamples.csv" # arquivos com as notas

# Metodo de execucao
isTrainingModel=True
isUseModel=False
steps = 10  # quantidade de redacoes por execucao

# Arquivos de Saida
grammarFileOutput = "featuresgrammar.fea"
NLPFileOutput = "featuresNLP.fea"
ortograficFileOutput = "featuresOrtografic.fea"
generalFileOutput = "featuresGeneral.fea"
datasetFileOutput = "dataset.dat"
tempMTXFileOutput = "dataset.mtx.temp"
MTXFileOutput = "dataset.mtx"
# Parametros default não mexer
headers = {'Content-Type': 'application/json','Content-Type': 'application/json; charset=utf-8'}
