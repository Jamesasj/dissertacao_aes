import sys
def to_mtx(entrada, saida, separador  ):
    arquivoEntrada = open(entrada, "r")
    linhasEntrada = arquivoEntrada.readlines()
    header = "%%Formato MTX\n"
    linhasSaida = ""
    qtdNaoNulos = 0
    i = 1
    for linha in linhasEntrada:
        j = 1
        _linha = linha.replace('\n', '')
        _linha = _linha.replace('\r\n', '')
        _linha = _linha.split(separador)
        for feature in _linha:     
            if feature != "0":
                qtdNaoNulos += 1
                linhasSaida += "%s %s %s\n" % (i, j, feature.replace('\n', '')) 
            j += 1
        i += 1
    arquivoSaida = open(saida, "w")
    header += "%d %d %d\n" % (len(linhasEntrada), linhasEntrada[0].count(separador) + 1, qtdNaoNulos)
    linhasSaida = header + linhasSaida
    arquivoSaida.write(linhasSaida)