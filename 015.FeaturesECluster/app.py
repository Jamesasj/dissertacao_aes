import requests, json, os, param
import threading, toMtx
import pandas as pd

def executarAnalise(list_dataessay, url, headers, file_output ):
  payload = json.dumps({'redacoes' : list_dataessay})
  fGrammar = requests.request("POST", url, headers=headers, data = payload)  
  file_output.write(fGrammar.text.replace("\n","").replace("],","\n").replace(".txt",",").replace("{","").replace("\"","").replace(":","").replace("[","").replace("}","").replace("]","").replace(" ",""))

def executarAnalise_v2(list_dataessay, url, headers, file_output , nameProcess):
  payload = json.dumps({'redacoes' : list_dataessay}).encode('utf8')
  response = requests.request("POST", url, headers=headers, data = payload)  
  responseJson = response.json()
  for k in responseJson.keys():
    file_output.write(responseJson[k]['features'] + '\n')
    file_output_info = open(param.dataset + "/" + str(k) + "." + str(nameProcess)+".fea", "a+",encoding='utf8')
    for erro in responseJson[k]['erros']:
      for key in erro:
        file_output_info.write( "%s : %s\n"%(key, erro[key]))
      file_output_info.write('\n') 
    file_output_info.close

def executeEssays(nameProcess,list_dataset_files, urlGrammar, idx, fileOutput):
  print("\t\tStart Execute "+nameProcess+ " whit node " + str(idx))
  list_dataessay = list()
  file_output = open(fileOutput, "a+")
  for file_name in list_dataset_files:
    f = open(param.dataset+file_name, 'r',encoding='utf8')
    list_dataessay.append({'texto' : f.read(), 'nomeArquivo':file_name})
    f.close()
    if(len(list_dataessay) == param.steps):
      executarAnalise_v2(list_dataessay, urlGrammar, param.headers, file_output, nameProcess)
      list_dataessay = list()
      file_output.write("\n")
  if(len(list_dataessay) > 0):
    executarAnalise_v2(list_dataessay, urlGrammar, param.headers, file_output, nameProcess)
    list_dataessay = list()
  file_output.close()
  print("\t\tEnd Execute "+ nameProcess+ "  whith node " + str(idx) )

def extractFeatures():
  print("Strat feature extraction... ")
  list_threads = []
  list_dataset_files = os.listdir(param.dataset)
  list_threads.append(threading.Thread(target = executeEssays, args=("Grammar", list_dataset_files, param.urlsGrammar[0], 0, param.grammarFileOutput,)))
  list_threads.append(threading.Thread(target = executeEssays, args=("NLP", list_dataset_files, param.urlsNLP[0], 0,param.NLPFileOutput,)))
  list_threads.append(threading.Thread(target = executeEssays , args=("Ortografic", list_dataset_files, param.urlsOrtografic[0], 0,param.ortograficFileOutput,)))
  list_threads.append(threading.Thread(target = executeEssays, args=("General", list_dataset_files, param.urlsGeneral[0], 0,param.generalFileOutput,)))
  print("\tExecute " +str(len(list_threads)) + " treads... ")
  for x in list_threads:
    x.start() 
  for x in list_threads:
    x.join()
  print("\tEnd of execution treads... ")
  print("\tEnd feature extraction... ")

def executeTraine():
  extractFeatures()
  print("Join Features ...")
  dsgrammar = pd.read_csv(param.grammarFileOutput, header = None)
  dsNLP = pd.read_csv(param.NLPFileOutput, header = None)
  dsOrtografic = pd.read_csv(param.ortograficFileOutput, header = None)
  dsGeneral = pd.read_csv(param.generalFileOutput, header = None)
  dsTemp = pd.merge(dsNLP,dsOrtografic,on=0)
  dsTemp2 = pd.merge(dsTemp,dsgrammar,on=0)
  dsFinal = pd.merge(dsTemp2,dsGeneral,on=0)

  #compression_opts = dict(method='zip',archive_name='out.csv')
  #t2.to_csv('out.zip', index=False, compression=compression_opts)
  dsTempMTX = dsFinal.drop(columns=0)
  dsTempMTX.to_csv(param.tempMTXFileOutput, index=False, header =False)
  dsFinal.to_csv(param.datasetFileOutput, index=False, header =False)
  toMtx.to_mtx(param.tempMTXFileOutput, param.MTXFileOutput, ",")


def executeEvaluation(param):
  pass

def main():
  if(param.isTrainingModel):
    executeTraine()
  if(param.isUseModel):
    executeEvaluation(param)

if __name__ == "__main__":
    main()